# readability-phantomjs

Use PhantomJS to drive [Readability.js](https://github.com/mozilla/readability) on a web site.

First, build PhantomJS from source:
```
git clone --depth=1 https://github.com/ariya/phantomjs.git
cd phantomjs && ./configure && make
```

And then, run it as follows (change the URL as necessary):
```
/path/to/bin/phantomjs drive-readability.js https://www.bbc.com/worklife/article/20190528-the-curious-origin-of-the-symbol
```