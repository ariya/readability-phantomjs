#!/usr/bin/env phantomjs

var system = require('system');
var webpage = require('webpage');

var address = null;
var verboseFlag = false;
var allowExtraResources = false;

function showHelp() {
    console.log('Usage: drive-readability.js [options] URL');
    console.log('');
    console.log('Possible options:');
    console.log('  --verbose                 Show some more additional information');
    console.log('  --allow-extra-resources   Allow loading extra resources (styles, scripts, etc');
    phantom.exit();
}

function verboseLog(str) {
    verboseFlag && console.log(str);
}

var args = system.args;
args.shift();
args.map(arg => {
    if (arg[0] === '-' && arg[1] === '-') {
        if (arg === '--verbose') {
            verboseFlag = true;
        } else if (arg === '--allow-extra-resources') {
            allowExtraResources = true;
        } else {
            console.log('Fatal error: unknown option ' + arg);
            phantom.exit();
        }
    } else {
        address = arg;
    }
});

if (address === null) {
    showHelp();
    phantom.exit();
}

var page = webpage.create();

page.settings.loadImages = false;

function inferContentType(requestHeaders) {
    var contentType = null;
    requestHeaders.forEach(function(header) {
        if (header.name && header.name === 'Accept') {
            if (header.value && header.value.length > 0) {
                contentType = header.value.split(',').shift();
            }
        }
    });
    return contentType;
}

function isThirdParty(url) {
    var blacklisted = [
        '2mdn.net',
        'adroll.com',
        'adservice.google.com',
        'akstat.io',
        'amazon-adsystem.com',
        'api.amplitude.com',
        'bbc-analytics',
        'bounceexchange.com',
        'brealtime.com',
        'captcha.js',
        'chartbeat',
        'connect.facebook.net',
        'crwdcntrl.net',
        'cxense.com',
        'demdex.net',
        'disqus.com',
        'doubleclick.net',
        'facebook.com/tr',
        'hotjar.com',
        'indexww.com',
        'instagram.com',
        'google-analytics.com',
        'googleadservices.com',
        'keywee.co',
        'krxd.net',
        'moatads.com',
        'ml314.com',
        'msecnd.net',
        'onetrust.com',
        'perimeterx.net',
        'platform.twitter.com',
        'polarcdn-',
        'quantserve.com',
        'recaptcha.net',
        'scorecardresearch.com',
        'optimizely.com',
        'youtube.com/embed/',
        'youtube.com/yts',
        'viafoura.net',
        'webcontentassessor.com'
    ];
    var matches = [];
    blacklisted.forEach(function(blocked) {
        if (url.indexOf(blocked) > 0) {
            matches.push(blocked);
        }
    });
    return matches;
}

function shouldBlock(requestData) {
    if (requestData.url && requestData.url.length > 0) {
        var contentType = inferContentType(requestData.headers);
        if (contentType === 'text/css') {
            verboseLog('  Blocking CSS ' + requestData.url);
            return true;
        } else if (contentType === '*/*') {
            var matches = isThirdParty(requestData.url);
            if (matches.length > 0) {
                verboseLog('  Blocking ' + requestData.url + ' due to ' + matches.join(' '));
                return true;
            }
            if (requestData.url.match(/woff$/i) || requestData.url.match(/woff2$/i) || requestData.url.match(/ttf$/i)) {
                verboseLog('  Blocking web font ' + requestData.url);
                return true;
            }
            if (requestData.url.match(/svg$/i)) {
                verboseLog('  Blocking SVG ' + requestData.url);
                return true;
            }
        }
    }
}

page.onResourceRequested = function(requestData, request) {
    if (allowExtraResources) {
        if (shouldBlock(requestData)) {
            request.abort();
        } else {
            verboseLog('  Retrieving: ' + requestData.url);
        }
    } else {
        const url = (requestData.url && requestData.url.length > 0) ? requestData.url : '';
        if (url.indexOf(address) >= 0 ) {
            verboseLog('  Retrieving: ' + requestData.url);
        } else {
            verboseLog('  Blocking EXTRA ' + requestData.url + ' vs ' + address);
            request.abort();
        }
    }
};

var start = Date.now();
verboseLog('Opening ' + address + '. Please wait...');

page.settings.userAgent = 'Mozilla/5.0 (Linux; Android 9) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36';

page.open(address, function(status) {
    if (status !== 'success') {
        verboseLog('FAIL to load the address!');
    } else {
        var elapsed = Date.now() - start;
        page.injectJs('Readability.js');
        var typeofReadability = page.evaluate(function () {
            return typeof Readability;
        });
        if (typeofReadability !== 'function') {
            verboseLog('FAIL to inject Readability.js!');
        } else {
            verboseLog('Readability.js is injected');
            verboseLog('Loading time ' + elapsed + ' msec');
            var parsedContent = page.evaluate(function() {
                return new Readability(document).parse();
            })
            console.log(parsedContent.title);
            console.log('');
            console.log(parsedContent.textContent);
        }
    }
    phantom.exit();
});