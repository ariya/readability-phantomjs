#!/usr/bin/env node

const child_process = require('child_process');
const fs = require('fs');

const URL_LIST_FILENAME = 'urls.list';
const TIMEOUT = 2 * 60 * 1000; // two minutes
const MAX_JOBS = 23;

function driveReadability(url, timeout, completed) {
    var content = '';
    var killed = false;
    const drive = child_process.spawn('phantomjs', ['drive-readability.js', url]);
    drive.stdout.on('data', (data) => {
        content += data.toString();
    });
    drive.on('close', (code) => {
        !killed && completed && completed('closed', url, content.trim());
        drive.kill();
    });
    setTimeout(() => {
        killed = true;
        drive.kill();
        completed && completed('killed', url, content.trim());
    }, timeout);
}

function getURLList(fname) {
    var contents = fs.readFileSync(fname, 'utf-8');
    var lines = contents.split('\n');
    var result = [];
    lines.forEach(line => {
        var url = line.trim();
        if (url.length > 0) {
            result.push(url);
        }
    });
    return result;
}

const urlList = getURLList(URL_LIST_FILENAME);
const totalUrls = urlList.length;

const jobs = [];
const completed = [];
const failed = [];

const contents = {};
urlList.slice().sort().forEach(url => contents[url] = '');

const start = Date.now();

function tick() {
    while ((urlList.length > 0) && (jobs.length < MAX_JOBS)) {
        var targetUrl = urlList.shift().trim();
        if (targetUrl.length > 0) {
            jobs.push(targetUrl);
            console.log('Going to ' + targetUrl);
            driveReadability(targetUrl, TIMEOUT, (reason, url, content) => {
                completed.push(url);
                var index = jobs.indexOf(url);
                if (index >= 0) {
                    jobs.splice(index, 1);
                }
                if (reason === 'closed') {
                    console.log('Success: ' + url);
                    contents[url] = content;
                } else {
                    console.log('Fail: ' + url);
                    failed.push(url);
                }
            });
        }
    }
    if (completed.length < totalUrls) {
        console.log('Still progressing: ' + (completed.length) + ' of ' + totalUrls);
    } else {
        const elapsed = Date.now() - start;
        console.log('COMPLETED in ' + Math.round(elapsed / 1000) + ' seconds.');
        fs.writeFileSync('contents-b.json', JSON.stringify(contents, null, 2));
        if (failed.length > 0) {
            console.log('Page(s) with failures/time-out:');
            console.log(failed.join('\n'));
        }
        process.exit();
    }
}

console.log('EXPERIMENT (c): Readability + PhantomJS 3, without loading any extra resources at all');
console.log('Number of parallel jobs: ' + MAX_JOBS);
console.log('Page timeout for each job, before it is terminated: ' + Math.round(TIMEOUT / 1000.0) + ' seconds');
console.log('');
console.log('Total URLs to be loaded (from ' + URL_LIST_FILENAME + '): ' + totalUrls);
console.log('About to process the following URLs:');
console.log(urlList.slice().sort().join('\n'));
console.log('');

setInterval(tick, 1000);