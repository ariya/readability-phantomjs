#!/usr/bin/env phantomjs

var fs = require('fs');
var webpage = require('webpage');

const URL_LIST_FILENAME = 'urls.list';
const TIMEOUT = 2 * 60 * 1000; // two minutes
const MAX_JOBS = 23;

var verboseFlag = false;

function verboseLog(str) {
    verboseFlag && console.log(str);
}

function getURLList(fname) {
    var contents = fs.read(fname);
    var lines = contents.split('\n');
    var result = [];
    lines.forEach(line => {
        var url = line.trim();
        if (url.length > 0) {
            result.push(url);
        }
    });
    return result;
}

const urlList = getURLList(URL_LIST_FILENAME);
const totalUrls = urlList.length;

const contents = {};
urlList.slice().sort().forEach(url => contents[url] = '');

console.log('EXPERIMENT (d): Readability + PhantomJS 3, without loading any extra resources at all');
console.log('Number of simultaneous page loading: ' + MAX_JOBS);
console.log('Page timeout for each job, before it is terminated: ' + Math.round(TIMEOUT / 1000.0) + ' seconds');
console.log('');
console.log('Total URLs to be loaded (from ' + URL_LIST_FILENAME + '): ' + totalUrls);
console.log('About to process the following URLs:');
console.log(urlList.slice().sort().join('\n'));
console.log('');

const jobs = urlList.slice();
const completed = [];
const failed = [];

const start = Date.now();

jobs.forEach(address => {
    var page = webpage.create();
    page.settings.loadImages = false;

    page.onResourceRequested = function(requestData, request) {
        const url = (requestData.url && requestData.url.length > 0) ? requestData.url : '';
        if (url.indexOf(address) >= 0 ) {
            verboseLog('  Retrieving: ' + requestData.url);
        } else {
            verboseLog('  Blocking EXTRA ' + requestData.url + ' vs ' + address);
            request.abort();
        }
    };

    page.settings.userAgent = 'Mozilla/5.0 (Linux; Android 9) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36';

    page.open(address, function(status) {
        if (status !== 'success') {
            verboseLog('FAIL to load the address!');
            failed.push(address);
        } else {
            var elapsed = Date.now() - start;
            page.injectJs('Readability.js');
            var typeofReadability = page.evaluate(function () {
                return typeof Readability;
            });
            if (typeofReadability !== 'function') {
                verboseLog('FAIL to inject Readability.js!');
                failed.push(address);
            } else {
                verboseLog('Readability.js is injected');
                var parsedContent = page.evaluate(function() {
                    return new Readability(document).parse();
                })
                contents[address] = parsedContent.textContent;
                completed.push(address);
            }
        }
    });
});

function tick() {
    if (completed.length < totalUrls) {
        console.log('Still progressing: ' + (completed.length) + ' of ' + totalUrls);
    } else {
        const elapsed = Date.now() - start;
        console.log('COMPLETED in ' + Math.round(elapsed / 1000) + ' seconds.');
        fs.write('contents-d.json', JSON.stringify(contents, null, 2));
        if (failed.length > 0) {
            console.log('Page(s) with failures/time-out:');
            console.log(failed.join('\n'));
        }
        phantom.exit();
    }
}

setInterval(tick, 1000);
