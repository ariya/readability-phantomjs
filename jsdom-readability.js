#!/usr/bin/env node

const fs = require('fs');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;

var address = null;

function showHelp() {
    console.log('Usage: jsdom-readability.js URL');
}

var args = process.argv;
args.shift();
args.shift();
args.map(arg => {
    if (arg[0] === '-' && arg[1] === '-') {
        console.log('Fatal error: unknown option ' + arg);
        process.exit();
    } else {
        address = arg;
    }
});

if (address === null) {
    showHelp();
    process.exit();
}


const readabilityScript = fs.readFileSync('Readability.js', 'utf-8');
eval(readabilityScript);

JSDOM.fromURL(address, {}).then(dom => {
    const parsedContent = new Readability(dom.window.document).parse();
    console.log(parsedContent.title);
    console.log('');
    console.log(parsedContent.textContent);
    process.exit();
});
